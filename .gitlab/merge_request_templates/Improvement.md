Merge King: Marge



---

- [ ] Does not contain project-specific code
- [ ] Tests updated/written
- [ ] Documentation updated/written


<!-- A link to a new issue or merge request to add or update moose-frank in the originated project to use your new code. -->
Issue/merge request in the originating project: 


/label ~improvement
