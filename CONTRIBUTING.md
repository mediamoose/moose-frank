# Contributing to Moose Frank

## Goal

The goal of this package is to have a place for much used Python code which has
to be easy to update. Therefor we have to bring in some guidelines to make this
an efficient process.

⚠️ Only add source code that is not unique to a particular project.


## Flow

Please follow the procedures below to make the process as efficient as possible.


### Adding code

When adding code make sure you follow these steps.

1. Create a WIP merge request in the original project which deletes the code you
   want to add here.
1. Create a merge request using `Addition.md` as merge request template for
   Moose Frank which contains the code. Fill in all fields.


### Improving code

When improving existing code make sure you follow these steps.

1. Create an issue or merge request in projects which use this code.
1. Create a merge request using `Improvement.md` as merge request template for
   Moose Frank which contains the improved code. Fill in all fields.


### Fixing bugs

When fixing a bug make sure you follow these steps.

1. Create a WIP merge request in all projects this bug occurs.
1. Create a merge request using `Bugfix.md` as merge request template for
   Moose Frank which fixes the bug. Fill in all fields.


### Deletion code

When deleting code make sure you follow these steps.

1. Create an issue or merge request in projects which use this code.
1. Create a merge request using `Deletion.md` as merge request template for
   Moose Frank which deletes the code. Fill in all fields.


