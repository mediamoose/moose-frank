��          |      �             !     8     N  3   j  0   �  4   �  @     1   E  =   w  #   �     �  ]  �     <     S     j  9   �  5   �  5   �  H   ,  5   u  ?   �  *   �                   
       	                                      %(height)d pixels high %(width)d pixels wide %(width)dx%(height)d pixels Image dimensions may not be equal to %(dimension)s. Image dimensions must be equal to %(dimension)s. Image dimensions must be greater than %(dimension)s. Image dimensions must be greater than or equal to %(dimension)s. Image dimensions must be less than %(dimension)s. Image dimensions must be less than or equal to %(dimension)s. Maximum file size is %(size).2g MB. page Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-08-16 10:24+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Mediamoose <projecten@mediamoose.nl>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %(height)d pixels hoog %(width)d pixels breed %(width)dx%(height)d pixels Beeldafmetingen mogen niet gelijk zijn aan %(dimension)s. Beeldafmetingen moeten gelijk zijn aan %(dimension)s. Beeldafmetingen moeten groter zijn dan %(dimension)s. Beeldafmetingen moeten groter zijn dan of gelijk zijn aan %(dimension)s. Beeldafmetingen moeten minder zijn dan %(dimension)s. Beeldafmetingen moeten minder of gelijk zijn aan %(dimension)s. Maximale bestandsgrootte is %(size).2g MB. pagina 