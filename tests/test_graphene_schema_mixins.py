from unittest.mock import MagicMock, patch

import graphene
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from django.test import SimpleTestCase

from moose_frank.graphene import schema_mixins


class CanDeleteMixinTestCase(SimpleTestCase):
    def test_can_delete_attribute(self):
        mixin = schema_mixins.CanDeleteMixin()
        assert isinstance(mixin.can_delete, graphene.Boolean)

    @patch("moose_frank.graphene.schema_mixins.NestedObjects")
    def test_resolve_can_delete(self, m):
        mixin = schema_mixins.CanDeleteMixin()
        mock = m.return_value

        mock.protected = True
        assert mixin.resolve_can_delete(info=None) is False
        mock.protected = False
        assert mixin.resolve_can_delete(info=None) is True


class UploadedFilesMixinTestCase(SimpleTestCase):
    class BaseClass:
        @classmethod
        def get_form_kwargs(cls, *args, **kwargs):
            return {"data": {}, "super_called": True}

    class UploadedFilesView(schema_mixins.UploadedFilesMixin, BaseClass):
        def __new__(cls, *args, **kwargs):
            cls._meta = MagicMock(model=MagicMock(singleton=lambda: "model"))
            return cls

    def test_get_form_kwargs(self):
        view = self.UploadedFilesView()
        assert view.get_form_kwargs(None, None) == {
            "files": {},
            "data": {},
            "super_called": True,
        }

    def test_get_form_kwargs_with_items(self):
        uploaded_file = UploadedFile(ContentFile(b"foo", "test.txt"))
        view = self.UploadedFilesView()
        items = {
            "0": {"file": uploaded_file},
            "1": {"file": uploaded_file, "clear": True},
            "2": "Foo",
            "3": {"file": "Foo"},
        }
        assert view.get_form_kwargs(None, None, **items) == {
            "files": {"0": uploaded_file, "1": uploaded_file},
            "data": {"1-clear": True},
            "super_called": True,
        }


class SingletonFormMixinTestCase(SimpleTestCase):
    class SingletonFormView(schema_mixins.SingletonFormMixin):
        def __new__(cls, *args, **kwargs):
            cls._meta = MagicMock(model=MagicMock(singleton=lambda: "model"))
            return cls

    def test_get_form_kwargs(self):
        view = self.SingletonFormView()
        assert view.get_form_kwargs(None, None) == {"data": {}, "instance": "model"}

    def test_get_form_kwargs_with_data(self):
        view = self.SingletonFormView()
        assert view.get_form_kwargs(None, None, id=10, foo="bar") == {
            "data": {"foo": "bar"},
            "instance": "model",
        }
