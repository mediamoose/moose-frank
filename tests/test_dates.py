from datetime import date, datetime, timedelta

from django.test import override_settings
from django.test.testcases import SimpleTestCase
from django.utils import timezone
from freezegun import freeze_time

from moose_frank.utils import get_age, get_date_overlap


class GetDateOverlapTestCase(SimpleTestCase):
    def test_no_overlap_empty_list(self):
        overlap = get_date_overlap([])
        assert not hasattr(overlap, "__dict__")
        assert overlap._fields == ("exists", "index1", "index2")

        assert overlap == (False, None, None)
        assert overlap.exists is False
        assert overlap.index1 is None
        assert overlap.index2 is None

    def test_overlap(self):
        assert get_date_overlap([(date(2016, 8, 1), date(2016, 8, 5))]).exists is False
        assert get_date_overlap([(date(2016, 8, 1), date(2016, 8, 1))]).exists is False

        assert (
            get_date_overlap(
                [
                    (date(2016, 8, 1), date(2016, 8, 5)),
                    (date(2016, 8, 10), date(2016, 8, 15)),
                ]
            ).exists
            is False
        )

        assert (
            get_date_overlap(
                [
                    (date(2016, 8, 1), date(2016, 8, 5)),
                    (date(2016, 8, 10), date(2016, 8, 15)),
                    (date(2016, 8, 16), date(2016, 8, 20)),
                ]
            ).exists
            is False
        )

        assert get_date_overlap(
            [(date(2016, 8, 1), date(2016, 8, 5)), (date(2016, 8, 1), date(2016, 8, 5))]
        ) == (True, 0, 1)

        assert get_date_overlap(
            [(date(2016, 8, 1), date(2016, 8, 5)), (date(2016, 8, 3), date(2016, 8, 8))]
        ) == (True, 0, 1)

        assert (
            get_date_overlap(
                [
                    (date(2016, 8, 1), date(2016, 8, 5)),
                    (date(2016, 7, 28), date(2016, 8, 3)),
                ]
            )
            == (True, 0, 1)
        )

        assert get_date_overlap(
            [(date(2016, 8, 1), date(2016, 8, 5)), (date(2016, 8, 5), date(2016, 8, 5))]
        ) == (True, 0, 1)

        assert (
            get_date_overlap(
                [
                    (date(2016, 8, 1), date(2016, 8, 5)),
                    (date(2016, 7, 28), date(2016, 8, 1)),
                ]
            )
            == (True, 0, 1)
        )

        assert (
            get_date_overlap(
                [
                    (date(2016, 8, 1), date(2016, 8, 5)),
                    (date(2016, 9, 28), date(2016, 12, 1)),
                    (date(2016, 12, 1), date(2016, 12, 10)),
                ]
            )
            == (True, 1, 2)
        )


class GetAgeTestCase(SimpleTestCase):
    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=False)
    def test_get_age(self):
        assert get_age(date(1988, 5, 9)) == 28
        assert get_age(date(1988, 5, 8)) == 28
        assert get_age(date(1988, 5, 7)) == 29
        assert get_age(date(1988, 5, 6)) == 29

        assert get_age(date(1988, 1, 1)) == 29
        assert get_age(date(1988, 12, 5)) == 28

        assert get_age(date(2017, 5, 6)) == 0
        assert get_age(date(2017, 5, 7)) == 0

    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=False)
    def test_get_age_with_datetime(self):
        assert get_age(datetime(1988, 5, 9, 12, 15, 2)) == 28
        assert get_age(datetime(1988, 5, 8, 12, 15, 2)) == 28
        assert get_age(datetime(1988, 5, 7, 12, 15, 2)) == 29
        assert get_age(datetime(1988, 5, 6, 12, 15, 2)) == 29

    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=False)
    def test_get_age_at_date(self):
        assert get_age(date(1988, 5, 9), at_date=date(1988, 5, 9)) == 0
        assert get_age(date(1988, 5, 9), at_date=date(1988, 5, 10)) == 0

        assert get_age(date(1988, 5, 9), at_date=date(1988, 5, 9)) == 0
        assert get_age(date(1988, 5, 9), at_date=date(1988, 5, 10)) == 0

        assert get_age(date(1988, 5, 9), at_date=date(1989, 5, 8)) == 0
        assert get_age(date(1988, 5, 9), at_date=date(1989, 5, 9)) == 1

        assert get_age(date(1988, 5, 9), at_date=date(2017, 5, 8)) == 28
        assert get_age(date(1988, 5, 9), at_date=date(2017, 5, 9)) == 29
        assert get_age(date(1988, 5, 9), at_date=date(2017, 8, 29)) == 29

    @override_settings(USE_TZ=False)
    def test_get_age_without_freezetime(self):
        assert get_age(date(1988, 5, 8)) >= 31

    @override_settings(USE_TZ=False)
    def test_get_age_without_freezetime_with_datetime(self):
        assert get_age(datetime(1988, 5, 8, 23, 7, 10)) >= 31

    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=True)
    def test_get_age_with_tz(self):
        assert get_age(date(1988, 5, 9)) == 28
        assert get_age(date(1988, 5, 8)) == 28
        assert get_age(date(1988, 5, 7)) == 29
        assert get_age(date(1988, 5, 6)) == 29

        assert get_age(date(1988, 1, 1)) == 29
        assert get_age(date(1988, 12, 5)) == 28

        assert get_age(date(2017, 5, 6)) == 0
        assert get_age(date(2017, 5, 7)) == 0

    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=True)
    def test_get_age_with_datetime_with_tz(self):
        assert get_age(datetime(1988, 5, 9, 12, 15, 2)) == 28
        assert get_age(datetime(1988, 5, 8, 12, 15, 2)) == 28
        assert get_age(datetime(1988, 5, 7, 12, 15, 2)) == 29
        assert get_age(datetime(1988, 5, 6, 12, 15, 2)) == 29

    @override_settings(USE_TZ=True)
    def test_get_age_without_freezetime_with_tz(self):
        assert get_age(date(1988, 5, 8)) >= 31

    @override_settings(USE_TZ=True)
    def test_get_age_without_freezetime_with_datetime_with_tz(self):
        assert get_age(datetime(1988, 5, 8, 23, 7, 10)) >= 31

    @freeze_time("2017-05-07 08:00")
    @override_settings(USE_TZ=True)
    def test_get_age_with_aware_birth_date(self):
        assert get_age(timezone.now()) == 0
        assert get_age(timezone.now() - timedelta(days=365)) == 1
