from unittest.mock import MagicMock, patch

from django.test import RequestFactory, SimpleTestCase
from graphene import Field, ObjectType, Schema

from moose_frank.graphene import types


class ThumbnailHelper:
    def get_thumbnail(self, *args, **kwargs):
        mock = MagicMock()
        mock.url = "thumbnail.jpeg"
        return mock

    def get_async_thumbnail(self, *args, **kwargs):
        mock = MagicMock()
        mock.url = "async/thumbnail.jpeg"
        return mock


class TestTypesTestCase(SimpleTestCase):
    @classmethod
    def setUpClass(cls):
        factory = RequestFactory()
        cls.request = factory.get("/")
        super().setUpClass()

    def test_file_object_type(self):
        file = types.FileObjectType()
        file.name = ""

        class Query(ObjectType):
            document = Field(types.FileObjectType)

            def resolve_document(self, info):
                return file

        schema = Schema(query=Query)
        result = schema.execute(
            """ query basequery {
            document {
                filename
                url
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {"document": {"filename": "", "url": ""}}

        file.name = "path/to/file.pdf"

        result = schema.execute(
            """ query basequery {
            document {
                filename
                url
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {
            "document": {"filename": "file.pdf", "url": "http://testserver/"}
        }

    def test_thumbnail_image_object_type_without_options(self):
        image = types.ImageObjectType()
        image.name = "path/to/image.jpeg"
        image.url = "image.jpeg"

        class Query(ObjectType):
            picture = Field(types.ImageObjectType)

            def resolve_picture(self, info):
                return image

        schema = Schema(query=Query)
        result = schema.execute(
            """ query basequery {
            picture {
                filename
                url
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {
            "picture": {"filename": "image.jpeg", "url": "http://testserver/image.jpeg"}
        }, result.data

    def test_thumbnail_image_object_type_without_name(self):
        image = types.ImageObjectType()
        image.name = ""

        class Query(ObjectType):
            picture = Field(types.ImageObjectType)

            def resolve_picture(self, info):
                return image

        schema = Schema(query=Query)
        result = schema.execute(
            """ query basequery {
            picture {
                filename
                url
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {"picture": {"filename": "", "url": ""}}, result.data

    @patch("moose_frank.graphene.types._get_thumbnail")
    def test_thumbnail_image_object_type_thumbnail(self, thumbnail):
        thumbnail.side_effect = ThumbnailHelper().get_thumbnail
        image = types.ImageObjectType()
        image.name = "path/to/image.jpeg"
        image.url = "image.jpeg"

        class Query(ObjectType):
            picture = Field(types.ImageObjectType)

            def resolve_picture(self, info):
                return image

        schema = Schema(query=Query)
        result = schema.execute(
            """ query basequery {
            picture {
                filename
                url(
                    options: {
                        geometry: "300x300"
                        crop: "center"
                    }
                )
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {
            "picture": {
                "filename": "image.jpeg",
                "url": "http://testserver/thumbnail.jpeg",
            }
        }, result.data

    @patch("moose_frank.graphene.types._get_async_thumbnail")
    def test_thumbnail_image_object_type(self, async_thumbnail):
        async_thumbnail.side_effect = ThumbnailHelper().get_async_thumbnail
        image = types.ImageObjectType()
        image.name = "path/to/image.jpeg"
        image.url = "image.jpeg"

        class Query(ObjectType):
            picture = Field(types.ImageObjectType)

            def resolve_picture(self, info):
                return image

        schema = Schema(query=Query)
        result = schema.execute(
            """ query basequery {
            picture {
                filename
                url(
                    options: {
                        geometry: "300x300"
                        crop: "center"
                        asynchronous: true
                    }
                )
            }
        }
        """,
            context=self.request,
        )
        assert not result.errors
        assert result.data == {
            "picture": {
                "filename": "image.jpeg",
                "url": "http://testserver/async/thumbnail.jpeg",
            }
        }, result.data
