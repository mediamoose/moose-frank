from django.core.exceptions import ImproperlyConfigured
from django.test import SimpleTestCase

from moose_frank.graphene import utils


def test_method():
    return True


class TestGrapheneUtilsTestCase(SimpleTestCase):
    def test_get_module_method(self):
        assert utils._get_module_method("") is None
        method = utils._get_module_method("test_graphene_utils.test_method")
        assert method.__name__ == "test_method"
        assert method() is True

        with self.assertRaises(ImproperlyConfigured):
            utils._get_module_method("does.not.exists")
