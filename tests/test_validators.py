from os.path import abspath, dirname, join
from typing import Optional
from unittest.mock import mock_open, patch

from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.test.testcases import SimpleTestCase

from moose_frank.tests.pillow import get_image
from moose_frank.utils.validators import FileSizeValidator, ImageDimensionsValidator


def create_mock_file(name: Optional[str] = "mock_file.txt") -> ContentFile:
    file_path = abspath(join(dirname(__file__), name))
    with patch("builtins.open", mock_open(read_data="data")):
        with open(file_path, "rb") as f:
            return ContentFile(f.read(), name)


class FileSizeValidatorTestCase(SimpleTestCase):
    def test_no_file_size(self):
        value = create_mock_file()
        validator = FileSizeValidator()
        assert validator(value) is None

    def test_file_size(self):
        value = create_mock_file()
        validator = FileSizeValidator(1024 * 1024)

        assert validator(value) is None

        value.size = 1024 * 1024
        assert validator(value) is None

        value.size = 1024 * 1024 + 1
        with self.assertRaises(ValidationError):
            validator(value)

    def test_validation_error_message(self):
        value = create_mock_file()
        value.size = 1024 * 1024 * 2
        validator = FileSizeValidator(1024, "Test")

        with self.assertRaisesMessage(ValidationError, "Test"):
            validator(value)

        validator = FileSizeValidator(1024 * 1024, "Test %(size).2g MB")

        with self.assertRaisesMessage(ValidationError, "Test 1 MB"):
            validator(value)

    def test_validation_error_code(self):
        value = create_mock_file()
        validator = FileSizeValidator(1)

        try:
            validator(value)
        except ValidationError as e:
            assert e.code == "invalid_file_size"

        validator = FileSizeValidator(1, code="invalid_value")

        try:
            validator(value)
        except ValidationError as e:
            assert e.code == "invalid_value"

    def test_equals(self):
        assert FileSizeValidator() == FileSizeValidator()

        assert FileSizeValidator(1024) != FileSizeValidator()
        assert FileSizeValidator(1024) != FileSizeValidator(1000)
        assert FileSizeValidator(1024) == FileSizeValidator(1024)

        assert FileSizeValidator(1024, "Test") != FileSizeValidator()
        assert FileSizeValidator(1024, "Test") != FileSizeValidator(1024, "Tes")
        assert FileSizeValidator(1024, "Test") == FileSizeValidator(1024, "Test")

        assert FileSizeValidator(1024, "Test", "invalid") != FileSizeValidator()
        assert FileSizeValidator(1024, "Test", "invalid") != FileSizeValidator(
            1024, "Test", "invali"
        )
        assert FileSizeValidator(1024, "Test", "invalid") == FileSizeValidator(
            1024, "Test", "invalid"
        )


class ImageDimensionsValidatorTestCase(SimpleTestCase):
    def test_no_width_or_height(self):
        value = get_image()
        validator = FileSizeValidator()
        assert validator(value) is None

    def test_open_file_when_closed(self):
        value = get_image(width=130, height=56)
        with patch("django.core.files.base.ContentFile.closed") as mock:
            mock.return_value = True
            validator = ImageDimensionsValidator(131, 56)
            with self.assertRaises(ValidationError):
                validator(value)

    def test_dimensions_validation(self):
        value = get_image(width=130, height=56)

        validator = ImageDimensionsValidator(130, 56)
        assert validator(value) is None

        validator = ImageDimensionsValidator(131, 56)
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(130, 57)
        with self.assertRaises(ValidationError):
            validator(value)

        # width
        validator = ImageDimensionsValidator(width=130)
        assert validator(value) is None

        validator = ImageDimensionsValidator(width=131)
        with self.assertRaises(ValidationError):
            validator(value)

        # height
        validator = ImageDimensionsValidator(height=56)
        assert validator(value) is None

        validator = ImageDimensionsValidator(height=57)
        with self.assertRaises(ValidationError):
            validator(value)

    def test_operator(self):
        value = get_image(width=130, height=56)

        # greater than
        validator = ImageDimensionsValidator(131, 57, op="ge")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(130, 56, op="ge")
        assert validator(value) is None

        validator = ImageDimensionsValidator(130, 56, op="gt")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(129, 55, op="gt")
        assert validator(value) is None

        # lower than
        validator = ImageDimensionsValidator(129, 56, op="le")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(130, 56, op="le")
        assert validator(value) is None

        validator = ImageDimensionsValidator(130, 56, op="lt")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(131, 57, op="lt")
        assert validator(value) is None
        assert validator(value) is None

        # equals
        validator = ImageDimensionsValidator(129, 56, op="eq")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(130, 56, op="eq")
        assert validator(value) is None

        validator = ImageDimensionsValidator(130, 56, op="ne")
        with self.assertRaises(ValidationError):
            validator(value)

        validator = ImageDimensionsValidator(131, 57, op="ne")
        assert validator(value) is None

    def test_validation_error_message(self):
        value = get_image(width=130, height=56)
        validator = ImageDimensionsValidator(150, message="Test")

        with self.assertRaisesMessage(ValidationError, "Test"):
            validator(value)

        validator = ImageDimensionsValidator(150, message="Test %(dimension)s")

        with self.assertRaisesMessage(ValidationError, "Test 150 pixels wide"):
            validator(value)

        validator = ImageDimensionsValidator(height=150, message="Test %(dimension)s")

        with self.assertRaisesMessage(ValidationError, "Test 150 pixels high"):
            validator(value)

        validator = ImageDimensionsValidator(150, 150, message="Test %(dimension)s")

        with self.assertRaisesMessage(ValidationError, "Test 150x150 pixels"):
            validator(value)

    def test_validation_error_code(self):
        value = get_image(width=130, height=56)
        validator = ImageDimensionsValidator(150)

        try:
            validator(value)
        except ValidationError as e:
            assert e.code == "invalid_image_dimensions"

        validator = ImageDimensionsValidator(150, code="invalid_value")

        try:
            validator(value)
        except ValidationError as e:
            assert e.code == "invalid_value"

    def test_equals(self):
        assert ImageDimensionsValidator() == ImageDimensionsValidator()

        assert ImageDimensionsValidator(width=150) != ImageDimensionsValidator()
        assert ImageDimensionsValidator(width=150) != ImageDimensionsValidator(
            width=100
        )
        assert ImageDimensionsValidator(width=150) == ImageDimensionsValidator(
            width=150
        )

        assert ImageDimensionsValidator(height=150) != ImageDimensionsValidator()
        assert ImageDimensionsValidator(height=150) != ImageDimensionsValidator(
            height=100
        )
        assert ImageDimensionsValidator(height=150) == ImageDimensionsValidator(
            height=150
        )

        assert ImageDimensionsValidator(op="") == ImageDimensionsValidator()
        assert ImageDimensionsValidator(op="test") == ImageDimensionsValidator()
        assert ImageDimensionsValidator(op="lt") != ImageDimensionsValidator(op="le")
        assert ImageDimensionsValidator(op="lt") == ImageDimensionsValidator(op="lt")

        assert ImageDimensionsValidator(message="Test") != ImageDimensionsValidator()
        assert ImageDimensionsValidator(message="Test") != ImageDimensionsValidator(
            message="Tes"
        )
        assert ImageDimensionsValidator(message="Test") == ImageDimensionsValidator(
            message="Test"
        )

        assert ImageDimensionsValidator(code="invalid") != ImageDimensionsValidator()
        assert ImageDimensionsValidator(code="invalid") != ImageDimensionsValidator(
            code="invali"
        )
        assert ImageDimensionsValidator(code="invalid") == ImageDimensionsValidator(
            code="invalid"
        )
