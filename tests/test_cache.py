from unittest.mock import Mock, patch

from django.core.cache.backends.locmem import LocMemCache
from django.test.testcases import SimpleTestCase

from moose_frank.utils.cache import cache_func, make_fragment_key


class CacheUtilsTestCase(SimpleTestCase):
    def test_make_fragment_key(self):
        key = make_fragment_key("cache_key_%s", "cache", "key")
        assert key == "cache_key_4eb08660b8f5a641862bda65e91a0c04"

        key = make_fragment_key("cache_key_%s", None, "key")
        assert key == "cache_key_61f5cf8cdf0203a693bc1d62e11d73b9"

        key = make_fragment_key("cache_key_%s", ["list", "argument"])
        assert key == "cache_key_88696cd8640578ac96e89863bbf05945"

    def test_cache_func(self):
        cache_key = make_fragment_key("test_cache.func_to_cache.%s", (), {})
        cache_key_with_args = make_fragment_key(
            "test_cache.func_to_cache.%s", ("test",), {}
        )
        cache_key_with_kwargs = make_fragment_key(
            "test_cache.func_to_cache.%s", ("test",), {"a": "b"}
        )
        mock = Mock()
        mock.return_value = "Test"

        @cache_func(60)
        def func_to_cache(*args, **kwargs):
            def check_called():
                return mock()

            return check_called()

        loc_mem_cache = LocMemCache(self.__class__.__name__, {})
        loc_mem_cache.clear()

        with patch("moose_frank.utils.cache.cache", loc_mem_cache):
            assert cache_key not in loc_mem_cache
            func_to_cache()
            assert cache_key in loc_mem_cache
            assert mock.called is True

            mock.reset_mock()
            func_to_cache()
            assert mock.called is False

            loc_mem_cache.clear()
            assert cache_key not in loc_mem_cache
            func_to_cache()
            assert cache_key in loc_mem_cache
            assert mock.called is True

            mock.reset_mock()
            assert cache_key_with_args not in loc_mem_cache
            func_to_cache("test")
            assert cache_key_with_args in loc_mem_cache
            assert mock.called is True

            mock.reset_mock()
            func_to_cache("test")
            assert mock.called is False

            mock.reset_mock()
            assert cache_key_with_kwargs not in loc_mem_cache
            func_to_cache("test", a="b")
            assert cache_key_with_kwargs in loc_mem_cache
            assert mock.called is True

            mock.reset_mock()
            func_to_cache("test", a="b")
            assert mock.called is False
