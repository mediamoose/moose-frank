import decimal
from decimal import Decimal
from unittest.mock import patch

from django import forms
from django.http.request import QueryDict
from django.test import override_settings
from django.test.client import RequestFactory
from django.test.testcases import SimpleTestCase
from django.utils import translation

from moose_frank.templatetags import active, generic, mathtags


class GenericTagsTestCase(SimpleTestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_filter_build_params(self):
        context = {"request": self.factory.get("/")}
        result = generic.filter_build_params(context, "param", "val")

        assert result == "?param=val"

        context = {"request": self.factory.get("/?abc=efg")}
        result = generic.filter_build_params(context, "param", "val")

        assert result == "?param=val"

        context = {"request": self.factory.get("/?param=val")}
        result = generic.filter_build_params(context)

        assert result == ""

        context = {"request": self.factory.get("/?ot=asc&o=title")}
        result = QueryDict(generic.filter_build_params(context, "param", "val")[1:])

        assert result["ot"] == "asc"
        assert result["o"] == "title"
        assert result["param"] == "val"

    def test_filter_active(self):
        context = {"request": self.factory.get("/?name=test")}
        assert generic.filter_active(context, "name", "test") == "active"
        assert generic.filter_active(context, "name", "different") == ""
        assert generic.filter_active(context, "name", "test", "checked") == "checked"
        assert generic.filter_active(context, "name", "different", "checked") == ""

    def test_paginator_params(self):
        request = self.factory.get("/")

        context = {"request": request}
        result = generic.paginator_get_params(context, 1)

        assert result == u"?page=1"

        context["request"] = self.factory.get("/?page=5")
        assert QueryDict(generic.paginator_get_params(context, 2)) == QueryDict(
            u"?page=2"
        )

        # TODO fix assert, random fail key order
        # context['request'] = self.factory.get('/?page=3&lorem=ipsum')
        # assert QueryDict(generic.paginator_get_params(
        #     context, 3)) == QueryDict(u'?page=3&lorem=ipsum')

        context["request"] = self.factory.get("/?page=50&_pjax=1")
        assert QueryDict(generic.paginator_get_params(context, 10)) == QueryDict(
            u"?page=10"
        )

    def test_form_action_params(self):
        class DummyForm(forms.Form):
            chars = forms.CharField()
            email = forms.EmailField()

        form = DummyForm()

        request = self.factory.get("/")

        context = {"request": request}
        result = generic.form_action_params(context, form)

        assert result == u"?"

        context["request"] = self.factory.get("/?chars=abc")
        assert generic.form_action_params(context, form) == u"?"

        context["request"] = self.factory.get("/?chars=abc&lorem=ipsum&foo=1")
        assert generic.form_action_params(context, form, "foo") == u"?lorem=ipsum"

        context["request"] = self.factory.get("/?email=qwerty&_pjax=1&chars=a")
        assert generic.form_action_params(context, form) == u"?"

    def test_replace(self):
        assert generic.replace("", None) == ""
        assert generic.replace("", "") == ""
        assert generic.replace("", "a") == ""
        assert generic.replace("", "a,") == ""
        assert generic.replace(None, "b") == ""
        assert generic.replace("abcdef", "bcd,") == "aef"
        assert generic.replace("abcdef", "bcd,xyz") == "axyzef"

    def test_clean_url(self):
        c = generic.clean_url

        assert c("") == ""
        assert c(None) == ""
        assert c("None") == ""
        assert c("abcdef") == "abcdef"
        assert c("localhost.nl") == "localhost.nl"
        assert c("localhost.nl/") == "localhost.nl"
        assert c("localhost.nl/?test") == "localhost.nl"
        assert c("www.localhost.nl") == "www.localhost.nl"
        assert c("www.localhost.nl/") == "www.localhost.nl"
        assert c("www.localhost.nl/?test=a") == "www.localhost.nl"
        assert c("www.localhost.nl/abc/def/") == "www.localhost.nl"
        assert c("www.localhost.nl/abc/") == "www.localhost.nl"
        assert c("www.localhost.nl/abc/?lorem=1") == "www.localhost.nl"
        assert c("http://localhost.nl/abc/") == "localhost.nl"
        assert c("http://localhost.nl/abc/?a=/") == "localhost.nl"
        assert c("http://sub.localhost.nl/abc/") == "sub.localhost.nl"
        assert c("https://sub.localhost.nl/abc/") == "sub.localhost.nl"

    def test_trunc_number_with_dot(self):
        assert generic.trunc_number_with_dot(None) == ""
        assert generic.trunc_number_with_dot("") == ""
        assert generic.trunc_number_with_dot(" ") == ""
        assert generic.trunc_number_with_dot([]) == "[]"

        assert generic.trunc_number_with_dot(0) == "0.00"
        assert generic.trunc_number_with_dot(1) == "1.00"
        assert generic.trunc_number_with_dot(1, 2) == "1.00"
        assert generic.trunc_number_with_dot(1, 3) == "1.000"
        assert generic.trunc_number_with_dot(1, decimals=2) == "1.00"

        assert generic.trunc_number_with_dot(12, decimals=2) == "12.00"
        assert generic.trunc_number_with_dot(12, decimals=3) == "12.000"
        assert generic.trunc_number_with_dot(-12, decimals=3) == "-12.000"

        assert generic.trunc_number_with_dot(float(1)) == "1.00"
        assert generic.trunc_number_with_dot(float(5.03)) == "5.03"
        assert generic.trunc_number_with_dot(float(1243.9823)) == "1243.98"
        assert generic.trunc_number_with_dot(float(1243.9823), 1) == "1243.9"
        assert generic.trunc_number_with_dot(float(1243.9823), 3) == "1243.982"
        assert generic.trunc_number_with_dot(float(1243.9823), 8) == "1243.98230000"
        assert generic.trunc_number_with_dot(float(-1243.9823), 3) == "-1243.982"

        assert generic.trunc_number_with_dot(64.999) == "64.99"
        assert generic.trunc_number_with_dot(64.999, 3) == "64.999"
        assert generic.trunc_number_with_dot(64.95) == "64.95"
        assert generic.trunc_number_with_dot(-64.95) == "-64.95"

        assert generic.trunc_number_with_dot(Decimal(0)) == "0.00"
        assert generic.trunc_number_with_dot(Decimal(1)) == "1.00"
        assert generic.trunc_number_with_dot(Decimal("64.999")) == "64.99"
        assert generic.trunc_number_with_dot(Decimal("64.999"), 1) == "64.9"
        assert generic.trunc_number_with_dot(Decimal("64.999"), 2) == "64.99"
        assert generic.trunc_number_with_dot(Decimal("64.999"), 3) == "64.999"
        assert generic.trunc_number_with_dot(Decimal("64.999"), 4) == "64.9990"
        assert generic.trunc_number_with_dot(Decimal("64.95")) == "64.95"
        assert generic.trunc_number_with_dot(Decimal("-64.95")) == "-64.95"

        assert generic.trunc_number_with_dot("0") == "0.00"
        assert generic.trunc_number_with_dot("0.00") == "0.00"
        assert generic.trunc_number_with_dot("0,00") == "0.00"
        assert generic.trunc_number_with_dot("10") == "10.00"
        assert generic.trunc_number_with_dot("10", 3) == "10.000"
        assert generic.trunc_number_with_dot("10.01", 1) == "10.0"
        assert generic.trunc_number_with_dot("10.01", 2) == "10.01"
        assert generic.trunc_number_with_dot("10.01", 3) == "10.010"
        assert generic.trunc_number_with_dot("10.01", 4) == "10.0100"
        assert generic.trunc_number_with_dot("10.01", 5) == "10.01000"

        assert generic.trunc_number_with_dot("1e+3") == "1000.00"
        assert generic.trunc_number_with_dot("1e-3") == "0.00"
        assert generic.trunc_number_with_dot("1.3e-3", 4) == "0.0013"
        assert generic.trunc_number_with_dot("1.32e-3", 4) == "0.0013"
        assert generic.trunc_number_with_dot("1.32e-3", 5) == "0.00132"

        assert generic.trunc_number_with_dot(1, None) == "1"
        assert generic.trunc_number_with_dot("1", None) == "1"
        assert generic.trunc_number_with_dot("1.5", None) == "1.5"

    def test_decimal_with_dot(self):
        assert generic.decimal_with_dot(None) == ""
        assert generic.decimal_with_dot("") == ""
        assert generic.decimal_with_dot(" ") == ""
        assert generic.decimal_with_dot([]) == "[]"

        assert generic.decimal_with_dot(0) == "0.00"
        assert generic.decimal_with_dot(1) == "1.00"
        assert generic.decimal_with_dot(1, 2) == "1.00"
        assert generic.decimal_with_dot(1, 3) == "1.000"
        assert generic.decimal_with_dot(1, decimals=2) == "1.00"

        assert generic.decimal_with_dot(12, decimals=2) == "12.00"
        assert generic.decimal_with_dot(12, decimals=3) == "12.000"
        assert generic.decimal_with_dot(-12, decimals=3) == "-12.000"

        assert generic.decimal_with_dot(float(1)) == "1.00"
        assert generic.decimal_with_dot(float(5.03)) == "5.03"
        assert generic.decimal_with_dot(float(1243.9823)) == "1243.9823"
        assert generic.decimal_with_dot(float(1243.9823), 1) == "1243.9823"
        assert generic.decimal_with_dot(float(1243.9823), 8) == "1243.98230000"

        assert generic.decimal_with_dot(64.999) == "64.999"
        assert generic.decimal_with_dot(64.999, 1) == "64.999"
        assert generic.decimal_with_dot(64.95) == "64.95"
        assert generic.decimal_with_dot(-64.95) == "-64.95"

        assert generic.decimal_with_dot(Decimal(0)) == "0.00"
        assert generic.decimal_with_dot(Decimal(1)) == "1.00"
        assert generic.decimal_with_dot(Decimal("64.999")) == "64.999"
        assert generic.decimal_with_dot(Decimal("64.999"), 1) == "64.999"
        assert generic.decimal_with_dot(Decimal("64.999"), 2) == "64.999"
        assert generic.decimal_with_dot(Decimal("64.999"), 3) == "64.999"
        assert generic.decimal_with_dot(Decimal("64.999"), 4) == "64.9990"
        assert generic.decimal_with_dot(Decimal("64.95")) == "64.95"
        assert generic.decimal_with_dot(Decimal("-64.95")) == "-64.95"

        assert generic.decimal_with_dot("0") == "0.00"
        assert generic.decimal_with_dot("0.00") == "0.00"
        assert generic.decimal_with_dot("0,00") == "0.00"
        assert generic.decimal_with_dot("10") == "10.00"
        assert generic.decimal_with_dot("10", 3) == "10.000"
        assert generic.decimal_with_dot("10.01", 1) == "10.01"
        assert generic.decimal_with_dot("10.01", 2) == "10.01"
        assert generic.decimal_with_dot("10.01", 3) == "10.010"
        assert generic.decimal_with_dot("10.01", 4) == "10.0100"
        assert generic.decimal_with_dot("10.01", 5) == "10.01000"

        assert generic.decimal_with_dot("1e+3") == "1000.00"
        assert generic.decimal_with_dot("1e-3") == "0.001"
        assert generic.decimal_with_dot("1.32e-3") == "0.00132"

        assert generic.decimal_with_dot(1, None) == "1"
        assert generic.decimal_with_dot("1", None) == "1"
        assert generic.decimal_with_dot("1.5", None) == "1.5"

    @override_settings(USE_THOUSAND_SEPARATOR=True, USE_L10N=True)
    def test_priceformat(self):
        assert generic.priceformat("") == ""
        assert generic.priceformat(None) == ""
        assert generic.priceformat(None, "1") == ""
        assert generic.priceformat(None, "0") == ""

        assert generic.priceformat(0, "0") == "0.-"
        assert generic.priceformat(0, None) == "0.-"
        assert generic.priceformat(0, "abcd") == "0.-"

        assert generic.priceformat(0) == "0.-"
        assert generic.priceformat(0, 0) == "0.-"
        assert generic.priceformat(0, 1) == "0.0"
        assert generic.priceformat(0, 2) == "0.00"

        assert generic.priceformat(0, 2) == "0.00"
        assert generic.priceformat(0, "2") == "0.00"

        assert generic.priceformat(0.0) == "0.-"
        assert generic.priceformat(0.0, 0) == "0.-"
        assert generic.priceformat(0.0, 1) == "0.0"
        assert generic.priceformat(0.0, 2) == "0.00"

        assert generic.priceformat(0.00) == "0.-"

        assert generic.priceformat(0.01) == "0.01"
        assert generic.priceformat(0.01, 0) == "0.-"
        assert generic.priceformat(0.01, 1) == "0.0"
        assert generic.priceformat(0.01, 2) == "0.01"

        assert generic.priceformat(0.001) == "0.-"
        assert generic.priceformat(0.001, 0) == "0.-"
        assert generic.priceformat(0.001, 1) == "0.0"
        assert generic.priceformat(0.001, 2) == "0.00"

        assert generic.priceformat(0.005) == "0.01"
        assert generic.priceformat(0.005, 0) == "0.-"
        assert generic.priceformat(0.005, 1) == "0.0"
        assert generic.priceformat(0.005, 2) == "0.01"
        assert generic.priceformat(0.005, 3) == "0.005"

        assert generic.priceformat(1) == "1.-"
        assert generic.priceformat(1, 0) == "1.-"
        assert generic.priceformat(1, 1) == "1.0"
        assert generic.priceformat(1, 2) == "1.00"

        assert generic.priceformat(1.0) == "1.-"
        assert generic.priceformat(1.00) == "1.-"
        assert generic.priceformat(1.001) == "1.-"
        assert generic.priceformat("1") == "1.-"
        assert generic.priceformat("1.0") == "1.-"
        assert generic.priceformat("1.00") == "1.-"

        assert generic.priceformat(12) == "12.-"
        assert generic.priceformat(12.00) == "12.-"
        assert generic.priceformat(12.000) == "12.-"
        assert generic.priceformat(12.50) == "12.50"
        assert generic.priceformat(12.51) == "12.51"
        assert generic.priceformat(12.510) == "12.51"
        assert generic.priceformat(12.60) == "12.60"
        assert generic.priceformat(12.01) == "12.01"

        assert generic.priceformat(500.1) == "500.10"
        assert generic.priceformat(500.10) == "500.10"
        assert generic.priceformat(500.104) == "500.10"
        assert generic.priceformat(500.105) == "500.11"
        assert generic.priceformat(500.1050) == "500.11"

        assert generic.priceformat(Decimal(672)) == "672.-"
        assert generic.priceformat(Decimal(672), 0) == "672.-"
        assert generic.priceformat(Decimal(672), 1) == "672.0"
        assert generic.priceformat(Decimal(672), 2) == "672.00"

        assert generic.priceformat(Decimal(672.00)) == "672.-"
        assert generic.priceformat(Decimal(672.5)) == "672.50"

        assert generic.priceformat(1000) == "1,000.-"
        assert generic.priceformat(1000.00) == "1,000.-"
        assert generic.priceformat(Decimal("1000")) == "1,000.-"
        assert generic.priceformat(Decimal("1000.00")) == "1,000.-"

        assert generic.priceformat(1000, 1) == "1,000.0"
        assert generic.priceformat(1000, 2) == "1,000.00"

        assert generic.priceformat(1000.005) == "1,000.01"
        assert generic.priceformat(1000.50) == "1,000.50"
        assert generic.priceformat(1001.50) == "1,001.50"

        assert generic.priceformat(1000000) == "1,000,000.-"
        assert generic.priceformat(1000000.00) == "1,000,000.-"
        assert generic.priceformat("1000000.00") == "1,000,000.-"
        assert generic.priceformat(1234567) == "1,234,567.-"
        assert generic.priceformat(1234567.00) == "1,234,567.-"

        with translation.override("nl"):
            assert generic.priceformat("") == ""
            assert generic.priceformat(None) == ""
            assert generic.priceformat(None, "1") == ""
            assert generic.priceformat(None, "0") == ""

            assert generic.priceformat(0, "0") == "0,-"
            assert generic.priceformat(0, None) == "0,-"
            assert generic.priceformat(0, "abcd") == "0,-"

            assert generic.priceformat(0) == "0,-"
            assert generic.priceformat(0, 0) == "0,-"
            assert generic.priceformat(0, 1) == "0,0"
            assert generic.priceformat(0, 2) == "0,00"

            assert generic.priceformat(0, 2) == "0,00"
            assert generic.priceformat(0, "2") == "0,00"

            assert generic.priceformat(0.0) == "0,-"
            assert generic.priceformat(0.0, 0) == "0,-"
            assert generic.priceformat(0.0, 1) == "0,0"
            assert generic.priceformat(0.0, 2) == "0,00"

            assert generic.priceformat(0.00) == "0,-"

            assert generic.priceformat(0.01) == "0,01"
            assert generic.priceformat(0.01, 0) == "0,-"
            assert generic.priceformat(0.01, 1) == "0,0"
            assert generic.priceformat(0.01, 2) == "0,01"

            assert generic.priceformat(0.001) == "0,-"
            assert generic.priceformat(0.001, 0) == "0,-"
            assert generic.priceformat(0.001, 1) == "0,0"
            assert generic.priceformat(0.001, 2) == "0,00"

            assert generic.priceformat(0.005) == "0,01"
            assert generic.priceformat(0.005, 0) == "0,-"
            assert generic.priceformat(0.005, 1) == "0,0"
            assert generic.priceformat(0.005, 2) == "0,01"
            assert generic.priceformat(0.005, 3) == "0,005"

            assert generic.priceformat(1) == "1,-"
            assert generic.priceformat(1, 0) == "1,-"
            assert generic.priceformat(1, 1) == "1,0"
            assert generic.priceformat(1, 2) == "1,00"

            assert generic.priceformat(1.0) == "1,-"
            assert generic.priceformat(1.00) == "1,-"
            assert generic.priceformat(1.001) == "1,-"
            assert generic.priceformat("1") == "1,-"
            assert generic.priceformat("1.0") == "1,-"
            assert generic.priceformat("1.00") == "1,-"

            assert generic.priceformat(12) == "12,-"
            assert generic.priceformat(12.00) == "12,-"
            assert generic.priceformat(12.000) == "12,-"
            assert generic.priceformat(12.50) == "12,50"
            assert generic.priceformat(12.51) == "12,51"
            assert generic.priceformat(12.510) == "12,51"
            assert generic.priceformat(12.60) == "12,60"
            assert generic.priceformat(12.01) == "12,01"

            assert generic.priceformat(500.1) == "500,10"
            assert generic.priceformat(500.10) == "500,10"
            assert generic.priceformat(500.104) == "500,10"
            assert generic.priceformat(500.105) == "500,11"
            assert generic.priceformat(500.1050) == "500,11"

            assert generic.priceformat(Decimal(672)) == "672,-"
            assert generic.priceformat(Decimal(672), 0) == "672,-"
            assert generic.priceformat(Decimal(672), 1) == "672,0"
            assert generic.priceformat(Decimal(672), 2) == "672,00"

            assert generic.priceformat(Decimal(672.00)) == "672,-"
            assert generic.priceformat(Decimal(672.5)) == "672,50"

            assert generic.priceformat(1000) == "1.000,-"
            assert generic.priceformat(1000.00) == "1.000,-"
            assert generic.priceformat(Decimal("1000")) == "1.000,-"
            assert generic.priceformat(Decimal("1000.00")) == "1.000,-"

            assert generic.priceformat(1000, 1) == "1.000,0"
            assert generic.priceformat(1000, 2) == "1.000,00"

            assert generic.priceformat(1000.005) == "1.000,01"
            assert generic.priceformat(1000.50) == "1.000,50"
            assert generic.priceformat(1001.50) == "1.001,50"

            assert generic.priceformat(1000000) == "1.000.000,-"
            assert generic.priceformat(1000000.00) == "1.000.000,-"
            assert generic.priceformat("1000000.00") == "1.000.000,-"
            assert generic.priceformat(1234567) == "1.234.567,-"
            assert generic.priceformat(1234567.00) == "1.234.567,-"

    def test_has_filter_parameter(self):
        assert not generic.has_filter_parameter(self.factory.get("/", data={}))
        assert not generic.has_filter_parameter(self.factory.get("/", data={"page": 1}))
        assert not generic.has_filter_parameter(self.factory.get("/", data={"foo": ""}))
        assert not generic.has_filter_parameter(
            self.factory.get("/", data={"foo": "", "page": 1})
        )

        assert generic.has_filter_parameter(self.factory.get("/", data={"foo": "1"}))
        assert generic.has_filter_parameter(self.factory.get("/", data={"foo": "abc"}))
        assert generic.has_filter_parameter(
            self.factory.get("/", data={"foo": ["a", "b", "c"]})
        )
        assert generic.has_filter_parameter(
            self.factory.get("/", data={"foo": "lorem", "page": 1})
        )

    def test_deunicode(self):
        inp = "é ë ê è ę ė ē"
        assert "e e e e e e e" == generic.deunicode(inp)
        inp = "á ä â ã å ā æ"
        assert "a a a a a a ae" == generic.deunicode(inp)
        inp = "ó ö ô ò õ ø ō œ"
        assert "o o o o o o o oe" == generic.deunicode(inp)


class ActiveTagsTestCase(SimpleTestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_active(self):
        assert active.active({"request": self.factory.get("/")}, "") == "active"

        assert active.active({"request": self.factory.get("/")}, "lorem") == ""
        assert (
            active.active({"request": self.factory.get("instellingen")}, "instellingen")
            == ""
        )

        assert (
            active.active(
                {"request": self.factory.get("/instellingen/")}, "/instellingen"
            )
            == "active"
        )

        assert (
            active.active(
                {"request": self.factory.get("/instellingen/")}, "/instellingen/"
            )
            == "active"
        )

        assert (
            active.active(
                {"request": self.factory.get("/instellingen/")},
                "/instellingen/",
                "huidig",
            )
            == "huidig"
        )

    def test_active_compare(self):
        assert active.active_compare("auto", "auto") == "active"
        assert active.active_compare("auto", "auto", "camper") == "active"

        assert active.active_compare("aut", "auto") == ""
        assert active.active_compare("aut", "auto", "camper") == ""

        assert active.active_compare("auto", "auto", class_name="current") == "current"
        assert (
            active.active_compare("auto", "auto", "active", class_name="current")
            == "current"
        )

    @patch("moose_frank.templatetags.active.reverse")
    def test_active_reverse(self, m):
        request = {"request": self.factory.get("/page/")}
        m.side_effect = lambda name, **kwargs: "/home/"
        assert active.active_reverse(request, "page:viewname") == ""

        m.side_effect = lambda name, **kwargs: "/page/"
        assert active.active_reverse(request, "other_page:viewname") == "active"
        assert (
            active.active_reverse(request, "other_page:viewname", class_name="current")
            == "current"
        )


class MathTagsTestCase(SimpleTestCase):
    def test_multiply(self):
        assert mathtags.mult(4, "2") == 8.0
        assert mathtags.mult(4, 2) == 8.0
        assert mathtags.mult(5.5, 2) == 11.0
        assert mathtags.mult(10, 2.5) == 20  # argument is cast to int

        with self.assertRaises(ValueError):
            mathtags.mult("", 1)

        with self.assertRaises(ValueError):
            mathtags.mult("abc", 2)

        with self.assertRaises(TypeError):
            mathtags.mult(None, 5)

        with self.assertRaises(ValueError):
            mathtags.mult(10, "abc")

        with self.assertRaises(ValueError):
            mathtags.mult(10, "")

        with self.assertRaises(TypeError):
            mathtags.mult(10, None)

    def test_subtract(self):
        assert mathtags.sub(4, "2") == 2.0
        assert mathtags.sub(4, 2) == 2.0
        assert mathtags.sub(5.5, 2) == 3.5
        assert mathtags.sub(10, 2.5) == 8  # argument is cast to int

        with self.assertRaises(ValueError):
            mathtags.sub("", 1)

        with self.assertRaises(ValueError):
            mathtags.sub("abc", 2)

        with self.assertRaises(TypeError):
            mathtags.sub(None, 5)

        with self.assertRaises(ValueError):
            mathtags.sub(10, "abc")

        with self.assertRaises(ValueError):
            mathtags.sub(10, "")

        with self.assertRaises(TypeError):
            mathtags.sub(10, None)

    def test_divide(self):
        assert mathtags.div(7, "2") == 3.5
        assert mathtags.div(4, 2) == 2.0
        assert mathtags.div(5.5, 2) == 2.75
        assert mathtags.div(10, 2.5) == 5  # argument is cast to int

        with self.assertRaises(ValueError):
            mathtags.div("", 1)

        with self.assertRaises(ValueError):
            mathtags.div("abc", 2)

        with self.assertRaises(TypeError):
            mathtags.div(None, 5)

        with self.assertRaises(ValueError):
            mathtags.div(10, "abc")

        with self.assertRaises(ValueError):
            mathtags.div(10, "")

        with self.assertRaises(TypeError):
            mathtags.div(10, None)

    def test_ceil(self):
        assert mathtags.ceil(7) == 7.0
        assert mathtags.ceil(4) == 4
        assert mathtags.ceil(5.5) == 6
        assert mathtags.ceil(5.4) == 6
        assert mathtags.ceil(10.01) == 11
        assert mathtags.ceil(10) == 10

        with self.assertRaises(ValueError):
            mathtags.ceil("")

        with self.assertRaises(ValueError):
            mathtags.ceil("abc")

        with self.assertRaises(TypeError):
            mathtags.ceil(None)

    def test_add_float(self):
        assert mathtags.addf(7, "2") == 9.0
        assert mathtags.addf(4, 2) == 6.0
        assert mathtags.addf(5.5, 2) == 7.5
        assert mathtags.addf(10, 2.5) == 12.5
        assert mathtags.addf(10.5, 2.5) == 13

        with self.assertRaises(ValueError):
            mathtags.addf("", 1)

        with self.assertRaises(ValueError):
            mathtags.addf("abc", 2)

        with self.assertRaises(TypeError):
            mathtags.addf(None, 5)

        with self.assertRaises(ValueError):
            mathtags.addf(10, "abc")

        with self.assertRaises(ValueError):
            mathtags.addf(10, "")

        with self.assertRaises(TypeError):
            mathtags.addf(10, None)

    def test_round(self):
        assert mathtags.round_decimal("8.8", 0) == 9
        assert mathtags.round_decimal("8.4", 0) == 8
        assert mathtags.round_decimal("8.43", "0.1") == Decimal("8.4")
        assert mathtags.round_decimal("8.48", "0.1") == Decimal("8.5")

        with self.assertRaises(decimal.InvalidOperation):
            mathtags.round_decimal("")

        with self.assertRaises(decimal.InvalidOperation):
            mathtags.round_decimal("abc")

        with self.assertRaises(TypeError):
            mathtags.round_decimal(None)
