from django.test.testcases import SimpleTestCase

from moose_frank.utils import strip_unicode


class StripUnicodeTestCase(SimpleTestCase):
    def test_strip_unicode(self):
        inp = "é ë ê è ę ė ē"
        assert "e e e e e e e" == strip_unicode(inp)
        inp = "á ä â ã å ā æ"
        assert "a a a a a a ae" == strip_unicode(inp)
        inp = "ó ö ô ò õ ø ō œ"
        assert "o o o o o o o oe" == strip_unicode(inp)
        inp = "í ï ì î į ī"
        assert "i i i i i i" == strip_unicode(inp)
        inp = "ú ü û ù ū"
        assert "u u u u u" == strip_unicode(inp)
        inp = "É Ë Ê È Ę Ė Ē"
        assert "E E E E E E E" == strip_unicode(inp)
        inp = "Á Ä Â Ã Å Ā Æ"
        assert "A A A A A A AE" == strip_unicode(inp)
        inp = "Ó Ö Ô Ò Õ Ø Ō Œ"
        assert "O O O O O O O OE" == strip_unicode(inp)
        inp = "Í Ï Ì Î Į Ī"
        assert "I I I I I I" == strip_unicode(inp)
        inp = "Ú Ü Û Ù Ū"
        assert "U U U U U" == strip_unicode(inp)
        inp = "ß"
        assert "ss" == strip_unicode(inp)

        assert "hastens" == strip_unicode("hästens")
        assert "hastens" == strip_unicode("hastens")
        assert "teutss" == strip_unicode("téütß")
        inp = "Klüft skräms inför på fédéral électoral große"
        outp = "Kluft skrams infor pa federal electoral grosse"
        assert outp == strip_unicode(inp)
