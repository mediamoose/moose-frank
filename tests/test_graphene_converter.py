from django.db import models
from django.forms import forms
from django.test import SimpleTestCase
from graphene import Field
from graphene_django.converter import convert_django_field
from graphene_django.forms.converter import convert_form_field

from moose_frank.graphene.converter import register_file_field
from moose_frank.graphene.types import FileObjectType, ImageObjectType, UploadInput


class TestRegisterFileFieldsTestCase(SimpleTestCase):
    @classmethod
    def setUpClass(cls):
        register_file_field()
        super().setUpClass()

    def test_should_convert_form_field_to_upload(self):
        field = forms.FileField(help_text="Custom Help Text")
        graphene_type = convert_form_field(field)
        assert isinstance(graphene_type, UploadInput)

        field = graphene_type.Field()
        assert field.description == "Custom Help Text"

    def test_should_convert_field_to_file_field(self):
        field = models.FileField(help_text="Custom Help Text", null=True)
        converted_field = convert_django_field(field)
        assert isinstance(converted_field, Field)
        assert converted_field.type == FileObjectType

    def test_should_convert_field_to_image_field(self):
        field = models.ImageField(help_text="Custom Help Text", null=True)
        converted_field = convert_django_field(field)
        assert isinstance(converted_field, Field)
        assert converted_field.type == ImageObjectType
