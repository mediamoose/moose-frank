ARG PYTHON_VERSION="3.8"

FROM python:$PYTHON_VERSION

RUN apt-get update \
 && apt-get install -y \
        gettext \
 && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /package/

COPY requirements.txt /package/requirements.txt
RUN pip install -r requirements.txt

ARG PIP_DJANGO="django"
RUN pip install $PIP_DJANGO

COPY . /package

RUN sed -i "s|{VERSION}|$(echo 1.0)|g" setup.py moose_frank/__init__.py
RUN pip install /package

RUN mkdir -p coverage
